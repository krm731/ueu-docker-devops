# Sarrera


## Lehen edukiontzia

```
docker run -p 8080:80 nginx
```

* `run`: Edukiontzi berrian komandoa exekutatu
* `-p 80:80`: (`--publish`) Edukiontziaren portua(k) zerbitzarian ireki
* `nginx`: Docker irudia, [https://hub.docker.com](https://hub.docker.com) irudi erregistro ofizialetik atzitua

::: tip ARIKETA
Bilatu `nginx` irudia erregistro ofizialean.
:::

## Erregistroa eta irudi tag-ak

* Docker erregistroa irudiak kudeatu eta zerbitzatzeko plataforma da.
* Software librea da (TODO link)
* Erregistro ezarpen ezberdinak daude (TODO link)

### Teknologia
TODO short description

### Irudi tag-ak
TODO description

## Edukiontzia gure fitxategiak erabiltzen

```
docker run --name www-zerb -p 8080:80 -v /edukiren/bat:/usr/share/nginx/html:ro -d nginx
```

* `--name www-zerb`: Edukiontzi berriaren izena
* `-v /edukiren/bat:/usr/share/nginx/html:ro`: (`--volume`) *Volume* bat lotu/muntatu
* `-d`: (`--detach`) Atzekaldean exekutatu eta edukiontzi ID-a itzuli

::: tip ARIKETA
Zerbitzatu HTML fitxategi bat (index.html) `nginx` irudi ofiziala erabilita.
:::

::: warning PROTIP
Aldagaiak erabili daitezke lasai oso, adibidez `$PWD`.
:::

::: tip ARIKETA
Zer gertatuko da aurreko komandoa exekutatuko bagenu?
:::

## Docker dokumentazioa

[https://docs.docker.com/engine/reference/commandline/cli/](https://docs.docker.com/engine/reference/commandline/cli/)
