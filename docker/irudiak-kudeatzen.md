# Irudiak kudeatzen

## docker images

Irudiak zerrendatzen ditu.

* `-a`: (`--all`) Tarteko irudiak baita zerrendatu
* `-q`: (`--quiet`) ID-ak bakarrik itzuli

## docker rmi

Irudiak ezabatzen ditu.

::: tip ARIKETA
Irudi guztiak ezabatu.
:::

::: tip TIP
Ikusi ditugun komado hauek `docker image`-ekoen *ezizenak* dira, exekutatu `docker help image` informazio guztia ikusteko.
:::

## docker pull

Irudiak atzitzeko erabiltzen da. Irudiak eguneratzeko balio du.

## docker image prune

[https://docs.docker.com/config/pruning/#prune-images](https://docs.docker.com/config/pruning/#prune-images)

Geldirik dauden edukiontziak ezabatu.
