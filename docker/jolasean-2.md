# Jolasean 2

Denbora eta animoen arabera software/aplikazioren bat *dockerizatzen* saia gaitezke.

Ideiarik?

## Praktika onak

* [eredu bat](https://nickjanetakis.com/blog/best-practices-when-it-comes-to-writing-docker-related-files)
* Zaindu nork exekutatzen duen komandoa. [`gosu`](https://github.com/tianon/gosu) / [`suexec`](https://github.com/ncopa/su-exec)
* Edukiontzi bat, prozesu bat (?)


## Conf fitxategi dinamikoak

* envsubst
* sed

## DevOps erremintak sortzen

(hipotesia)

``` docker
FROM node:alpine
RUN yarn global add vuepress
WORKDIR /workdir
USER node
EXPOSE 8080
ENTRYPOINT ["vuepress"]
```

```
docker build -t vuepress .
```

::: danger ARAZOA / ARIKETA

Badirudi `nodejs` bertsio berriago bat behar dugula!

Bertsio egokia erabiltzeko beharrezko aldaketak egin.

:::


```
docker run --rm -v $PWD:/workdir vuepress dev
```

::: danger ARAZOA / ARIKETA

U,o... ez du `vuepress` topatzen.

Debugeatu eta konponduko degu!

:::

```
docker run --rm --entrypoint sh vuepress
```

::: warning ARIKETA

Zer falta da? Zergatik ez digu `shell` bat irekitzen?

:::

::: danger HARDCORE

A zer  komeriak!

:::

::: tip ARIKETA

Orain badakigu zein den `vuepress` komandoaren bidea, konpondu, sortu irudia eta exekutatu!

:::

::: tip ARIKETA

Nola hobetuko zenuke komandoa?

:::

::: tip ARIKETA

Irudiaren geruzak nola katxeatzen diren ikusiko dugu.

:::

## Borobiltzen

Behin eta berriro komando luze hori idaztea nekeza eta zaila da, nola borobildu dezakegu hau? Bai! *shell script* bat sortuko dugu! (*alias* bat, nahaiago baduzu)

``` bash
#! /bin/sh
docker run --rm -p 8080:8080 -v $PWD:/workdir vuepress $1
```

Gorde lerro pare hori `$HOME/.local/bin/vuepress` (ala `$HOME/bin/vuepress` ubuntu berrian?) bezala, exekutagarri egin (`chmod +x`) eta ...

**Zorionak!** VuePress *dockerizatua* ari zara erabiltzen!


---

::: danger SPOILER

``` docker
FROM node:9-alpine

USER node

RUN yarn global add vuepress

WORKDIR /workdir

EXPOSE 8080

ENTRYPOINT ["/home/node/.yarn/bin/vuepress"]
```
